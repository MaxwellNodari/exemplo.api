﻿using AutoMapper;
using Exemplo.Api.DbContexts.Entities;
using Exemplo.Api.Models.DTOs;

namespace Exemplo.Api.Models.Mappers
{
    public class BoletoAutoMapperProfile : Profile
    {
        public BoletoAutoMapperProfile()
        {
            CreateMap<BoletoDTO, BoletoEntity>()
              .ReverseMap();
        }
    }
}
