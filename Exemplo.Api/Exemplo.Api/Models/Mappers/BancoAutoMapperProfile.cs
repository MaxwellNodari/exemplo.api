﻿using AutoMapper;
using Exemplo.Api.DbContexts.Entities;
using Exemplo.Api.Models.DTOs;

namespace Exemplo.Api.Models.Mappers
{
    public class BancoAutoMapperProfile : Profile
    {
        public BancoAutoMapperProfile()
        {
            CreateMap<BancoDTO, BancoEntity>()
              .ReverseMap();
        }
    }
}
