﻿using System.ComponentModel.DataAnnotations;

namespace Exemplo.Api.Models.DTOs
{
    public class BancoDTO
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "O campo 'Nome do Banco' é obrigatório.")]
        public string NomeBanco { get; set; }

        [Required(ErrorMessage = "O campo 'Código do Banco' é obrigatório.")]
        public string CodigoBanco { get; set; }

        [Required(ErrorMessage = "O campo 'Percentual de Juros' é obrigatório.")]
        [Range(0, double.MaxValue, ErrorMessage = "O campo 'Percentual de Juros' deve ser maior ou igual a zero.")]
        public decimal PercentualJuros { get; set; }
    }
}
