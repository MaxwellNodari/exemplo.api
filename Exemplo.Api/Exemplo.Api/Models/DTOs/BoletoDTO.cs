﻿using System.ComponentModel.DataAnnotations;

namespace Exemplo.Api.Models.DTOs
{
    public class BoletoDTO
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "O campo 'Nome do Pagador' é obrigatório.")]
        public string NomePagador { get; set; }

        [Required(ErrorMessage = "O campo 'CPF/CNPJ do Pagador' é obrigatório.")]
        public string CPFCNPJPagador { get; set; }

        [Required(ErrorMessage = "O campo 'Nome do Beneficiário' é obrigatório.")]
        public string NomeBeneficiario { get; set; }

        [Required(ErrorMessage = "O campo 'CPF/CNPJ do Beneficiário' é obrigatório.")]
        public string CPFCNPJBeneficiario { get; set; }

        [Required(ErrorMessage = "O campo 'Valor' é obrigatório.")]
        [Range(0, double.MaxValue, ErrorMessage = "O campo 'Valor' deve ser maior ou igual a zero.")]
        public decimal Valor { get; set; }

        [Required(ErrorMessage = "O campo 'Data de Vencimento' é obrigatório.")]
        public DateTime DataVencimento { get; set; }

        public string Observacao { get; set; }

        [Required(ErrorMessage = "O campo 'BancoId' é obrigatório.")]
        public int BancoId { get; set; }
    }
}
