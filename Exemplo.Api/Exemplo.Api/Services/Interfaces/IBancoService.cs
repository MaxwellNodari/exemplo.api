﻿using Exemplo.Api.Models.DTOs;

namespace Exemplo.Api.Services.Interfaces
{
    public interface IBancoService
    {
        BancoDTO CreateBanco(BancoDTO bancoDTO);
        BancoDTO GetBancoById(int id);
        List<BancoDTO> GetAllBancos();
        BancoDTO GetBancoByCodigo(string codigo);
    }
}
