﻿using Exemplo.Api.Models.DTOs;

namespace Exemplo.Api.Services.Interfaces
{
    public interface IBoletoService
    {
        BoletoDTO CreateBoleto(BoletoDTO boletoDTO);
        BoletoDTO GetBoletoById(int id);
        List<BoletoDTO> GetAllBoletos();
    }
}
