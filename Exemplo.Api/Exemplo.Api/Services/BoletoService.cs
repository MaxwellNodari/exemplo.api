﻿using AutoMapper;
using Exemplo.Api.DbContexts.Entities;
using Exemplo.Api.Exceptions;
using Exemplo.Api.Models.DTOs;
using Exemplo.Api.Repositories.Interfaces;
using Exemplo.Api.Services.Interfaces;

namespace Exemplo.Api.Services
{
    public class BoletoService : IBoletoService
    {
        private readonly IBoletoRepository _boletoRepository;
        private readonly IMapper _mapper;
        private readonly IBancoService _bancoService;

        public BoletoService(IBoletoRepository boletoRepository, IMapper mapper, IBancoService bancoService)
        {
            _boletoRepository = boletoRepository;
            _mapper = mapper;
            _bancoService = bancoService;
        }

        public BoletoDTO CreateBoleto(BoletoDTO boletoDTO)
        {
            var boletoEntity = _mapper.Map<BoletoEntity>(boletoDTO);
            return _mapper.Map<BoletoDTO>(_boletoRepository.CreateBoleto(boletoEntity));
        }

        public List<BoletoDTO> GetAllBoletos()
        {
            var boletos = _mapper.Map<List<BoletoDTO>>(_boletoRepository.GetAllBoletos());

            foreach (var boleto in boletos)
            {
                CalcularJuros(boleto);
            }

            return boletos;
        }

        public BoletoDTO GetBoletoById(int id)
        {
            var boleto = _mapper.Map<BoletoDTO>(_boletoRepository.GetBoletoById(id));
            ValidarNotFound(boleto);
            CalcularJuros(boleto);

            return boleto;
        }

        private void CalcularJuros(BoletoDTO boleto)
        {
            if (boleto.DataVencimento < DateTime.Now)
            {
                var banco = _bancoService.GetBancoById(boleto.BancoId);
                if (banco != null)
                {
                    decimal juros = boleto.Valor * (banco.PercentualJuros / 100);
                    boleto.Valor += juros;
                }
            }
        }

        private static void ValidarNotFound(BoletoDTO bancos)
        {
            if (bancos == null)
                throw new NotFoundException();
        }
    }
}
