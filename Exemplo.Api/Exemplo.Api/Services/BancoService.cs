﻿using AutoMapper;
using Exemplo.Api.DbContexts.Entities;
using Exemplo.Api.Exceptions;
using Exemplo.Api.Models.DTOs;
using Exemplo.Api.Repositories.Interfaces;
using Exemplo.Api.Services.Interfaces;

namespace Exemplo.Api.Services
{
    public class BancoService : IBancoService
    {
        private readonly IBancoRepository _bancoRepository;
        private readonly IMapper _mapper;

        public BancoService(IBancoRepository bancoRepository, IMapper mapper)
        {
            _bancoRepository = bancoRepository;
            _mapper = mapper;
        }

        public BancoDTO CreateBanco(BancoDTO bancoDTO)
        {
           var bancoEntity = _mapper.Map<BancoEntity>(bancoDTO);
           return _mapper.Map<BancoDTO>(_bancoRepository.CreateBanco(bancoEntity));
        }

        public List<BancoDTO> GetAllBancos()
        {
            var bancos = _mapper.Map<List<BancoDTO>>(_bancoRepository.GetAllBancos());
            return bancos;
        }

        public BancoDTO GetBancoByCodigo(string codigo)
        {
            var bancos = _mapper.Map<BancoDTO>(_bancoRepository.GetBancoByCodigo(codigo));
            ValidarNotFound(bancos);
            return bancos;
        }

        public BancoDTO GetBancoById(int id)
        {
            var bancos = _mapper.Map<BancoDTO>(_bancoRepository.GetBancoById(id));
            ValidarNotFound(bancos);
            return bancos;
        }

        private static void ValidarNotFound(BancoDTO bancos)
        {
            if (bancos == null)
                throw new NotFoundException();
        }
    }
}
