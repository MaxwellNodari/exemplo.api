﻿using Exemplo.Api.DbContexts;
using Exemplo.Api.Models.DTOs;
using Exemplo.Api.Models.Mappers;
using Exemplo.Api.Repositories;
using Exemplo.Api.Repositories.Interfaces;
using Exemplo.Api.Services;
using Exemplo.Api.Services.Interfaces;
using Exemplo.Api.Validators;
using FluentValidation;
using FluentValidation.AspNetCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.OpenApi.Models;
namespace Exemplo.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<ApiDbContext>(options =>
            {
                options.UseInMemoryDatabase("TestDatabase");
            });

            // Adiciona o FluentValidation
            services.AddControllers()
             .AddFluentValidation(fv => fv.RegisterValidatorsFromAssemblyContaining<BancoDTOValidator>())
             .AddFluentValidation(fv => fv.RegisterValidatorsFromAssemblyContaining<BoletoDTOValidator>());

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "API Exemplo", Version = "v1" });

            });
            services.AddTransient<IValidator<BancoDTO>, BancoDTOValidator>();
            services.AddTransient<IValidator<BoletoDTO>, BoletoDTOValidator>();
            services.AddScoped<IBoletoService, BoletoService>();
            services.AddScoped<IBancoService, BancoService>();
            services.AddScoped<IBoletoRepository, BoletoRepository>();
            services.AddScoped<IBancoRepository, BancoRepository>();

            services.AddAutoMapper(typeof(BancoAutoMapperProfile));
            services.AddAutoMapper(typeof(BoletoAutoMapperProfile));
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();

                //using (var scope = app.ApplicationServices.CreateScope())
                //{
                //    var context = scope.ServiceProvider.GetRequiredService<ApiDbContext>();
                //    context.Database.Migrate();
                //}
            }


            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Exemplo.Api V1");
                c.RoutePrefix = string.Empty;
            });

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }


    }
}
