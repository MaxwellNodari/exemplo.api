﻿using Exemplo.Api.Models.DTOs;
using FluentValidation;

namespace Exemplo.Api.Validators
{
    public class BoletoDTOValidator : AbstractValidator<BoletoDTO>
    {
        public BoletoDTOValidator()
        {
            RuleFor(x => x.NomePagador).NotEmpty();
            RuleFor(x => x.CPFCNPJPagador).NotEmpty();
            RuleFor(x => x.NomeBeneficiario).NotEmpty();
            RuleFor(x => x.CPFCNPJBeneficiario).NotEmpty();
            RuleFor(x => x.Valor).NotEmpty().GreaterThan(0);
            RuleFor(x => x.DataVencimento).NotEmpty();
            RuleFor(x => x.BancoId).NotEmpty();
        }
    }
}
