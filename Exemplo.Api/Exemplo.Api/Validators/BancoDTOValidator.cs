﻿using Exemplo.Api.Models.DTOs;
using FluentValidation;

namespace Exemplo.Api.Validators
{
    public class BancoDTOValidator : AbstractValidator<BancoDTO>
    {
        public BancoDTOValidator()
        {
            RuleFor(x => x.NomeBanco).NotEmpty();
            RuleFor(x => x.CodigoBanco).NotEmpty();
            RuleFor(x => x.PercentualJuros).NotEmpty().GreaterThan(0);
        }
    }
}
