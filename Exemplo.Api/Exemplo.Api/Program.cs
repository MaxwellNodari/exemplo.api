using Exemplo.Api;

using var webhost = Microsoft.AspNetCore.WebHost.CreateDefaultBuilder(args)
    .UseStartup<Startup>()
    .UseKestrel()
    .UseUrls("http://0.0.0.0:5000")
    .Build();
webhost.Run();