﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Exemplo.Api.DbContexts.Entities
{
    [Table("Boletos")]
    public class BoletoEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string NomePagador { get; set; }
        public string CPFCNPJPagador { get; set; }
        public string NomeBeneficiario { get; set; }
        public string CPFCNPJBeneficiario { get; set; }
        public decimal Valor { get; set; }
        public DateTime DataVencimento { get; set; }
        public string Observacao { get; set; }
        public int BancoId { get; set; }
        public BancoEntity Banco { get; set; }
    }
}
