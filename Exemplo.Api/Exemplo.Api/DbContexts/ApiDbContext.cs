﻿using Exemplo.Api.DbContexts.Entities;
using Microsoft.EntityFrameworkCore;

namespace Exemplo.Api.DbContexts
{
    public class ApiDbContext : DbContext
    {
        public DbSet<BancoEntity> Bancos { get; set; }
        public DbSet<BoletoEntity> Boletos { get; set; }

        public ApiDbContext(DbContextOptions<ApiDbContext> options) : base(options)
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseInMemoryDatabase("TestDatabase");
            base.OnConfiguring(optionsBuilder);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<BancoEntity>(entity =>
            {
                entity.ToTable("Bancos");
                entity.HasKey(e => e.Id); 
                entity.Property(e => e.Id)
                    .ValueGeneratedOnAdd(); 
              
            });

            modelBuilder.Entity<BoletoEntity>(entity =>
            {
                entity.ToTable("Boletos"); 
                entity.HasKey(e => e.Id); 
                entity.Property(e => e.Id)
                    .ValueGeneratedOnAdd(); 
                
                entity.HasOne(e => e.Banco) 
                    .WithMany() 
                    .HasForeignKey(e => e.BancoId) 
                    .IsRequired(); 
            });

            //base.OnModelCreating(modelBuilder);
        }


    }
}
