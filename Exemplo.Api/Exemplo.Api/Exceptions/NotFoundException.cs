﻿namespace Exemplo.Api.Exceptions
{
    public class NotFoundException : Exception
    {
        public NotFoundException()
        : base($"Não foram encontrado registros.")
        {
        }
        public NotFoundException(string errorMessage)
            : base(errorMessage)
        {
        }
    }
}
