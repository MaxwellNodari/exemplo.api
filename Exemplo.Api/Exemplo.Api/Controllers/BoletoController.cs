﻿using Exemplo.Api.Exceptions;
using Exemplo.Api.Models.DTOs;
using Exemplo.Api.Services;
using Exemplo.Api.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace Exemplo.Api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class BoletoController : ControllerBase
    {
        private readonly IBoletoService _boletoService;

        public BoletoController(IBoletoService boletoService)
        {
            _boletoService = boletoService;
        }
        /// <summary>
        /// Busca todos os registros
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IActionResult GetAllBoletos()
        {
            try
            {
                var bancos = _boletoService.GetAllBoletos();
                return Ok(bancos);
            }
            catch (NotFoundException e)
            {
                return NotFound(e.Message);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }

        }


        /// <summary>
        /// Busca o registro filtrando pelo ID
        /// </summary>
        /// <param name="idBoleto"></param>
        /// <returns></returns>
        [HttpGet("Id/{idBoleto}")]
        public IActionResult GetBoletoById(int idBoleto)
        {
            try
            {
                var banco = _boletoService.GetBoletoById(idBoleto);
                return Ok(banco);
            }
            catch (NotFoundException e)
            {
                return NotFound(e.Message);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
        /// <summary>
        /// Insere um novo registro
        /// </summary>
        /// <param name="boletoDTO"></param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult CreateBoleto(BoletoDTO boletoDTO)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }
                var boleto = _boletoService.CreateBoleto(boletoDTO);
                return CreatedAtAction(nameof(GetBoletoById), new { id = boleto.Id }, boleto);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
    }
}
