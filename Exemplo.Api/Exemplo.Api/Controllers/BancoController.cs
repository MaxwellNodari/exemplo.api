﻿using Exemplo.Api.Exceptions;
using Exemplo.Api.Models.DTOs;
using Exemplo.Api.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;

namespace Exemplo.Api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class BancoController : ControllerBase
    {
        private readonly IBancoService _bancoService;

        public BancoController(IBancoService bancoService)
        {
            _bancoService = bancoService;
        }
        /// <summary>
        /// Busca todos os registros
        /// </summary>
        /// <returns></returns>
        [SwaggerOperation(Summary = "Buscar todos", Description = "Busca todos os registros.")]
        [HttpGet]
        public IActionResult GetAllBancos()
        {
            try
            {
                var bancos = _bancoService.GetAllBancos();
                return Ok(bancos);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
          
        }

        /// <summary>
        /// Busca o registro filtrando pelo código
        /// </summary>
        /// <param name="codigo"></param>
        /// <returns></returns>
        [SwaggerOperation(Summary = "Buscar por código", Description = "Busca o registro filtrando pelo código.")]
        [HttpGet("Codigo/{codigo}")]
        public IActionResult GetBancoByCodigo(string codigo)
        {
            try
            {
                var banco = _bancoService.GetBancoByCodigo(codigo);
                return Ok(banco);
            }
            catch (NotFoundException e)
            {
                return NotFound(e.Message);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
        /// <summary>
        /// Busca o registro filtrando pelo ID
        /// </summary>
        /// <param name="idBanco"></param>
        /// <returns></returns>
        [SwaggerOperation(Summary = "Buscar por id", Description = "Busca o registro filtrando pelo ID.")]
        [HttpGet("Id/{idBanco}")]
        public IActionResult GetBancoById(int idBanco)
        {
            try
            {
                var banco = _bancoService.GetBancoById(idBanco);
                return Ok(banco);
            }
            catch (NotFoundException e)
            {
                return NotFound(e.Message);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
        /// <summary>
        /// Insere um novo registro
        /// </summary>
        /// <param name="bancoDTO"></param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult CreateBanco(BancoDTO bancoDTO)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }
                var banco = _bancoService.CreateBanco(bancoDTO);
                return CreatedAtAction(nameof(GetBancoByCodigo), new { codigo = banco.CodigoBanco }, banco);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
    }
}
