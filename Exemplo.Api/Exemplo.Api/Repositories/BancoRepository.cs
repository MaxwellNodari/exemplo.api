﻿using Exemplo.Api.DbContexts;
using Exemplo.Api.DbContexts.Entities;
using Exemplo.Api.Repositories.Interfaces;

namespace Exemplo.Api.Repositories
{
    public class BancoRepository : IBancoRepository
    {
        private readonly ApiDbContext _context;

        public BancoRepository(ApiDbContext context)
        {
            _context = context;
        }

        public BancoEntity CreateBanco(BancoEntity banco)
        {
            _context.Bancos.Add(banco);
            _context.SaveChanges();
            return banco;
        }

        public List<BancoEntity> GetAllBancos()
        {
            var bancos = _context.Bancos.ToList();
            return  bancos;
        }

        public BancoEntity GetBancoByCodigo(string codigo)
        {
            var banco = _context.Bancos.FirstOrDefault(b => b.CodigoBanco == codigo);
            if (banco == null)
                return null;
            return banco;
        }

        public BancoEntity GetBancoById(int id)
        {
            var banco = _context.Bancos.FirstOrDefault(b => b.Id == id);
            if (banco == null)
                return null;
            return banco;
        }
    }
}
