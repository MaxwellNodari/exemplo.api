﻿using Exemplo.Api.DbContexts.Entities;

namespace Exemplo.Api.Repositories.Interfaces
{
    public interface IBoletoRepository
    {
        BoletoEntity CreateBoleto(BoletoEntity boleto);
        BoletoEntity GetBoletoById(int id);
        List<BoletoEntity> GetAllBoletos();
    }
}
