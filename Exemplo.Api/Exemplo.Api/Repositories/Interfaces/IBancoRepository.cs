﻿using Exemplo.Api.DbContexts.Entities;

namespace Exemplo.Api.Repositories.Interfaces
{
    public interface IBancoRepository
    {
        BancoEntity CreateBanco(BancoEntity banco);
        BancoEntity GetBancoById(int id);
        List<BancoEntity> GetAllBancos();
        BancoEntity GetBancoByCodigo(string codigo);
    }
}
