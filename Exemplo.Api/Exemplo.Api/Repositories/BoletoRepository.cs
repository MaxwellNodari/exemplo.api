﻿using Exemplo.Api.DbContexts;
using Exemplo.Api.DbContexts.Entities;
using Exemplo.Api.Repositories.Interfaces;

namespace Exemplo.Api.Repositories
{
    public class BoletoRepository : IBoletoRepository
    {
        private readonly ApiDbContext _context;

        public BoletoRepository(ApiDbContext context)
        {
            _context = context;
        }

        public BoletoEntity CreateBoleto(BoletoEntity boleto)
        {
            _context.Boletos.Add(boleto);
            _context.SaveChanges();
            return boleto;
        }

        public List<BoletoEntity> GetAllBoletos()
        {
            var boletos = _context.Boletos.ToList();
            return boletos; 
        }

        public BoletoEntity GetBoletoById(int id)
        {
            var boleto = _context.Boletos.FirstOrDefault(b => b.Id == id);
            if (boleto == null)
                return null;
            return boleto;
        }
    }
}
